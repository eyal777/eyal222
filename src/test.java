
public class test {

    public static void Print_Matrix(float[][] A) {
        for (int i = 0 ;i < A.length ;i++) {
            for (int j = 0 ;j < A[0].length ;j++)
                System.out.print(Math.round(A[i][j] * 10000.0) / 10000.0 + " ");
            System.out.println();
        }
        System.out.println();
    }

    private static boolean Is_Unit_Matrix(float[][] A) {
        int n = A.length;
        boolean flag = true;
        for(int i = 0 ;i < n ;i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    if (A[i][j] == 1) flag = true;
                    else {
                        flag = false;
                        break;
                    }
                } else {
                    if (A[i][j] == 0) flag = true;
                    else {
                        flag = false;
                        break;
                    }
                }
            }
            if(!flag) break;
        }
        return flag;
    }

    private static float[][] Mult_Mats(float[][] M1, float[][] M2) throws Exception{
        if(M1[0].length == M2.length){
            float[][] A = new float[M1.length][M2[0].length];
            for (int i = 0 ;i < M1.length ;i++)
                for (int j = 0 ;j < M2[0].length ;j++)
                    for (int k = 0 ;k < M2.length ;k++)
                        A[i][j] += M1[i][k] * M2[k][j];
            return A;
        }else throw new Exception ("Error: The sizes Required is not Conditions");
    }

    public static void main(String[] args) {
        float[][] M1 = {{1,3,2},{1,0,0},{1,2,2}};
        float[][] M2 = {{0,0,2},{7,5,0},{2,1,1}};
        try {
            float[][] M = Mult_Mats(M1,M2);
            Print_Matrix(M);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
