
public class Invertible_Mat_2 {

    public static void Print_Matrix(float[][] A) {
        for (int i = 0 ;i < A.length ;i++) {
            for (int j = 0 ;j < A[0].length ;j++)
                System.out.print(Math.round(A[i][j] * 10000.0) / 10000.0 + " ");
            System.out.println();
        }
        System.out.println();
    }

    private static float[][] Mult_Mats(float[][] M1, float[][] M2) throws Exception{
        if(M1[0].length == M2.length){
            float[][] A = new float[M1.length][M2[0].length];
            for (int i = 0 ;i < M1.length ;i++)
                for (int j = 0 ;j < M2[0].length ;j++)
                    for (int k = 0 ;k < M2.length ;k++)
                        A[i][j] += M1[i][k] * M2[k][j];
            return A;
        }else throw new Exception ("Error: The sizes Required is not Conditions");
    }

    private static void Retreat_Rows_Matrixs(float[][] M ,float[][] invM ,int r1 ,int r2){
        for(int j = 0 ;j < M[0].length ;j++){
            float t = M[r1][j];
            M[r1][j] = M[r2][j];
            M[r2][j] = t;
            float invt = invM[r1][j];
            invM[r1][j] = invM[r2][j];
            invM[r2][j] = invt;
        }
    }

    private static boolean Is_Unit_Vector(float[][] A ,int k){
        boolean flag = true;
        for(int i = 0 ;i < A.length ;i++) {
            if (A[k][i] != 0) {
                for (int j = 0; j < A.length; j++) {
                    if (j == i) continue;
                    else {
                        if (A[k][j] == 0) flag = true;
                        else {
                            flag = false;
                            break;
                        }
                    }
                }
                if (flag) break;
            }else continue;
        }
        return flag;
    }

    private static boolean Is_Unit_Matrix(float[][] M) {
        int n = M.length;
        boolean flag = true;
        for(int i = 0 ;i < n ;i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    if (M[i][j] == 1) flag = true;
                    else {
                        flag = false;
                        break;
                    }
                } else {
                    if (M[i][j] == 0) flag = true;
                    else {
                        flag = false;
                        break;
                    }
                }
            }
            if(!flag) break;
        }
        return flag;
    }

    public static float[][] Unit_Matrix(int n) {
        float[][] unit = new float[n][n];
        for (int i = 0 ;i < unit.length ;i++)
            for (int j = 0 ;j < unit[0].length ;j++) {
                if (i == j)
                    unit[i][j] = 1;
                else
                    unit[i][j] = 0;
            }
        return unit;
    }

    private static int Get_Index_UnZero(float[][] A ,int k){
        for(int i = k + 1 ;i < A.length ;i++)
            if(A[i][k] != 0) return i;
        return -1;
    }

    public static float[][] Inv_Iter(float[][] M) throws Exception {
        int n = M.length;
        float[][] invM = Unit_Matrix(n);
        int i = 0 ,j = 1;
        while(!Is_Unit_Matrix(M)){
            //float[][] E = Unit_Matrix(n);
            Print_Matrix(M);
            if(M[i][i] == 0) {
                int k = Get_Index_UnZero(M,i);
                System.out.println("R" + (i + 1) + " <--> R" + (k % n + 1) + "\n");
                Retreat_Rows_Matrixs(M ,invM ,i,k % n);
                //M = Mult_Mats(invM, M);
            }else{
                if(i != j){
                    if(M[j][i] != 0){
                        float c = M[j][i] / M[i][i];
                        if (c > 0) System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " - " + c + "*R" + (i + 1) + "\n");
                        else System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " + " + (-1) * c + "*R" + (i + 1) + "\n");
                        invM[j][i] = (-1) * c;
                        M = Mult_Mats(invM, M);
                        if(M[j][i] != 0) M[j][i] = 0;
                    }
                }else{
                    if(Is_Unit_Vector(M, j) && M[j][j] != 1){
                        System.out.println("R" + (j + 1) + " --> " + (1 / M[j][j]) + "*R" + (j + 1) + "\n");
                        invM[j][j] = 1 / M[j][j];
                        M = Mult_Mats(invM, M);
                        if(M[j][j] != 1) M[j][j] = 1;
                    }
                }
                if(j == n - 1) i = (i + 1)%n;
                j = (j + 1)%n;
            }
        }
        return invM;
    }

    public static void main(String[] args) {
        float[][] A = {{2,1,-1},{-3,-1,2},{-2,1,2}};
        float[][] E = {{1,1,-2,0,-1,-1},{0,-2,-1,2,-2,2},{0,0,-1,-1,-1,1},{0,0,-2,2,-2,0},{-2,-2,0,0,-2,2},{1,0,-1,0,0,2}};
        float[][] invE = new float[0][];
        try {
            invE = Inv_Iter(E);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Print_Matrix(invE);
    }
}
