import java.util.Vector;
import java.util.Scanner;

public class System_Linear_Equations {

    public static void Display_Exercise(float[][] A ,float[] b) {
        System.out.println("Solve The Next Exercise:");
        if(A[0].length == 1) System.out.println(A[0][0] + "*x = " + b[0]);
        else
            for (int i = 0 ;i < A.length ;i++) {
                for (int j = 0 ;j < A[0].length ;j++) {
                    System.out.print(A[i][j] + "*x" + (j + 1));
                    if(j == A[0].length - 1) System.out.print(" = ");
                    else System.out.print(" + ");
                }
                System.out.println(b[i]);
            }
        System.out.println();
    }

    private static void Print_State(float[][] A ,float[] b) {
        for (int i = 0 ;i < A.length ;i++) {
            for (int j = 0 ;j < A[0].length ;j++)
                System.out.print(Math.round(A[i][j] * 10000.0) / 10000.0 + " ");
            System.out.println("| " + Math.round(b[i] * 10000.0) / 10000.0);
        }
        System.out.println();
    }

    private static void Menu_User(){
        System.out.println("Choose Number Method To Solution:");
        System.out.println("1. Numeric Method");
        System.out.println("2. Cramer Method");
        System.out.println("3. Forward Backward Method");
        System.out.println("4. Ranking Method");
        System.out.println();
    }

    public static void Print_Matrix(float[][] A) {
        for (int i = 0 ;i < A.length ;i++) {
            for (int j = 0 ;j < A[0].length ;j++)
                System.out.print(Math.round(A[i][j] * 10000.0) / 10000.0 + " ");
            System.out.println();
        }
        System.out.println();
    }

    public static void Print_Vector(float[] b){
        for (int i = 0 ;i < b.length ;i++)
            System.out.println(Math.round(b[i] * 10000.0) / 10000.0 + " ");
        System.out.println();
    }

    private static boolean Is_Square_Matrix(float[][] A) {
        return A.length == A[0].length;
    }

    private static boolean Is_Length_Equals(float[][] A ,float[] b){ return A.length == b.length && A[0].length == b.length; }

    public static float[][] Transpose(float[][] A) {
        for (int i = 0 ;i < A.length ;i++) {
            for (int j = 0; j < i; j++) {
                float t = A[j][i];
                A[j][i] = A[i][j];
                A[i][j] = t;
            }
        }
        return A;
    }

    public static float Determine(float[][] A) {
        if (A.length == 1) return A[0][0];
        float sum = 0;
        int k = 1;
        for (int i = 0 ;i < A.length ;i++) {
            sum += A[0][i] * Determine(Sub_Matrix(A, 0, i)) * k;
            k = k * (-1);
        }
        return sum;
    }

    public static float[][] Sub_Matrix(float[][] A, int x, int y) {
        int n = A.length ,p = 0 ,q = 0;
        float[][] subA = new float[n - 1][n - 1];
        for (int i = 0 ;i < n ;i++) {
            for (int j = 0; j < n; j++) {
                if (i != x && j != y) {
                    subA[p][q] = A[i][j];
                    q++;
                    if (q == subA[0].length) {
                        p++;
                        q = 0;
                    }
                }
            }
        }
        return subA;
    }

    public static float[][] Adjoint(float[][] A) {
        int n = A.length;
        float[][] AT = new float[n][n];
        for (int i = 0 ;i < n ;i++) {
            for (int j = 0; j < n; j++) {
                if ((i + j) % 2 == 0) AT[i][j] = Determine(Sub_Matrix(A, i, j));
                else AT[i][j] = (-1) * Determine(Sub_Matrix(A, i, j));
            }
        }
        return Transpose(AT);
    }

    public static float[][] Invertible(float[][] A) {
        float[][] invA = new float[A.length][A.length];
        float[][] adj = Adjoint(A);
        for (int i = 0 ;i < A.length ;i++)
            for (int j = 0 ;j < A.length ;j++)
                invA[i][j] = (1 / Determine(A)) * adj[i][j];
        return invA;
    }

    private static float[][] Mult_Mats(float[][] L, float[][] U) throws Exception{
        if(L[0].length == U.length){
            float[][] A = new float[L.length][U[0].length];
            for (int i = 0 ;i < L.length ;i++)
                for (int j = 0 ;j < U[0].length ;j++)
                    for (int k = 0 ;k < U.length ;k++)
                        A[i][j] += L[i][k] * U[k][j];
            return A;
        }else throw new Exception ("Error: The sizes Required is not Conditions");
    }

    private static void Swap(float[]b ,int i ,int j){
        float t = b[i];
        b[i] = b[j];
        b[j] = t;
    }

    private static void Retreat_Rows(float[][] A ,int r1 ,int r2){
        for(int j = 0 ;j < A[0].length ;j++){
            float t = A[r1][j];
            A[r1][j] = A[r2][j];
            A[r2][j] = t;
        }
    }

    private static float[][] Copy_Matrix(float[][] A){
        float[][] copyA = new float[A.length][A.length];
        for (int i = 0 ;i < A.length ;i++)
            for (int j = 0 ;j < A[0].length ;j++)
                copyA[i][j] = A[i][j];
        return copyA;
    }

    public static float[][] Ranking_Matrix(float[][] A){
        int n = A.length;
        float[][] rankA = Copy_Matrix(A);
        for (int i = 0 ;i < n - 1 ;i++) {
            for (int j = i + 1; j < n; j++) {
                if (rankA[i][i] == 0) Retreat_Rows(rankA, i, (i + 1) % n);
                float c = rankA[j][i] / rankA[i][i];
                for (int k = 0; k < n; k++)
                    rankA[j][k] = rankA[j][k] - rankA[i][k] * c;
            }
        }
        return rankA;
    }

    public static float[] Sub_Vector(float[] b, int x){
        int n = b.length;
        float[] subb = new float[n - 1];
        for (int i = 0 ;i < b.length ;i++)
            if (i != x) subb[i] = b[i];
        return subb;
    }

    private static boolean Is_Zero_Matrix(float[][] A){
        boolean flag = true;
        for(int i = 0 ;i < A.length ;i++) {
            for (int j = 0; j < A[0].length; j++) {
                if(A[i][j] == 0) flag = true;
                else{
                    flag = false;
                    break;
                }
            }
            if(!flag) break;
        }
        return flag;
    }

    private static boolean Is_Zero_Vector(float[] b){
        boolean flag = true;
        for(int i = 0; i < b.length; i++){
            if(b[i] == 0) flag = true;
            else{
                flag = false;
                break;
            }
        }
        return flag;
    }

    private static boolean Is_Unit_Vector(float[][] A ,int k){
        boolean flag = true;
        for(int i = 0 ;i < A.length ;i++) {
            if (A[k][i] != 0) {
                for (int j = 0; j < A.length; j++) {
                    if (j == i) continue;
                    else {
                        if (A[k][j] == 0) flag = true;
                        else {
                            flag = false;
                            break;
                        }
                    }
                }
                if (flag) break;
            }else continue;
        }
        return flag;
    }

    private static int get_index_unZero(float[][] A ,int k){
        for(int i = 0 ;i < A.length ;i++)
            if(A[k][i] != 0) return i;
        return -1;
    }

    private static Vector<Float> Zero_Vector(int n){
        Vector<Float> v = new Vector<Float>();
        for(int k = 0 ;k < n ;k++)
            v.add((float) 0);
        return v;
    }

    private static boolean Is_Unit_Matrix(float[][] A) {
        int n = A.length;
        boolean flag = true;
        for(int i = 0 ;i < n ;i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    if (A[i][j] == 1) flag = true;
                    else {
                        flag = false;
                        break;
                    }
                } else {
                    if (A[i][j] == 0) flag = true;
                    else {
                        flag = false;
                        break;
                    }
                }
            }
            if(!flag) break;
        }
        return flag;
    }

    public static float[][] Unit_Matrix(int n) {
        float[][] unit = new float[n][n];
        for (int i = 0 ;i < unit.length ;i++)
            for (int j = 0 ;j < unit[0].length ;j++) {
                if (i == j)
                    unit[i][j] = 1;
                else
                    unit[i][j] = 0;
            }
        return unit;
    }

    private static Vector<Float> Invertible_Method(float[][] A, float[] b){
        int n = A.length;
        Vector<Float> v = Zero_Vector(n);
        float[] x = new float[n];
        float[][] invA = Invertible(A);
        System.out.println("the solution is: inv(A)*b\n");
        Print_Matrix(invA);
        Print_Vector(b);
        for(int i = 0 ;i < n ;i++){
            x[i] = 0;
            for(int j = 0 ;j < n ;j++)
                x[i] += b[j] * invA[i][j];
            v.set(i,(float) (Math.round(x[i] * 10000.0) / 10000.0));
        }
        return v;
    }

    private static Vector<Float> Cramer_Method(float[][] A, float[] b){
        int n = A.length;
        Vector<Float> v = Zero_Vector(n);
        float det = Determine(A);
        Print_Matrix(A);
        System.out.println("det(A) = " + det + "\n");
        float[] dets = new float[n];
        float[] helper = new float[n];
        for(int j = 0 ;j < n ;j++){
            for(int i = 0 ;i < n ;i++){
                helper[i] = A[i][j];
                A[i][j] = b[i];
            }
            dets[j] = Determine(A);
            Print_Matrix(A);
            System.out.println("det(A" + (j + 1) + ") = " + dets[j]);
            for(int i = 0 ;i < n ;i++)
                A[i][j] = helper[i];
            v.set(j,(float) (dets[j]/det));
            System.out.println("x" + (j + 1) + " = " + v.elementAt(j) + "\n");
        }
        return v;
    }

    private static Vector<Float> Forward_Backward_Method(float[][] A, float[] b) throws Exception{
        int n = b.length;
        Vector<Float> v = Zero_Vector(n);
        Print_State(A,b);
        System.out.println("Upper Ranking of A:\n");
        float[][] U = Ranking_Matrix(A);
        float[][] L = Mult_Mats(A,Invertible(U));
        for(int i = 0 ;i < n - 1 ;i++)
            for (int j = i + 1 ;j < n ;j++)
                if (L[i][i] == 0){
                    Retreat_Rows(L, i, j);
                    Swap(b, i, j);
                    break;
                }
        float[] x = new float[n];
        float[] y = new float[n];
        System.out.println("Forward solve Ly = b\n");
        Print_State(L,b);
        for(int i = 0; i < n; i++){
            y[i] = b[i];
            for(int j = 0; j < i; j++)
                y[i] -= L[i][j] * y[j];
            y[i] /= L[i][i];
        }
        System.out.println("Backward solve Ux = y\n");
        Print_State(U,y);
        for(int i = n - 1; i >= 0; i--){
            x[i] = y[i];
            for(int j = i + 1; j < n; j++)
                x[i] -= U[i][j] * x[j];
            x[i] /= U[i][i];
            v.set(i,(float) (Math.round(x[i] * 10000.0) / 10000.0));
        }
        return v;
    }

    private static Vector<Float> Ranking_Method(float[][] A ,float[] b){
        System.out.println("Transform A Matrix To I (Unit Matrix) By Elementary Actions:\n");
        int n = A.length;
        Vector<Float> v = Zero_Vector(n);
        Print_State(A,b);
        for (int i = 0 ;i < n ;i++) {
            for (int j = 0 ;j < n ;j++) {
                if(A[i][i] == 0 && i != (j + i + 1)%n){
                    System.out.println("R" + (i + 1) + " <--> R" + ((j + i + 1)%n + 1) + "\n");
                    Retreat_Rows(A,i,(j + i + 1)%n);
                    Swap(b,i,(j + i + 1)%n);
                    Print_State(A,b);
                }
                if(i != j){
                    if(A[j][i] == 0) {
                        if(Is_Unit_Vector(A,j)) {
                            if(A[j][j] == 0){
                                int k = get_index_unZero(A,j);
                                if(j != (k + j + 1)%n){
                                    System.out.println("R" + (j + 1) + " <--> R" + ((j + k + 1)%n + 1) + "\n");
                                    Retreat_Rows(A,j,(k + j + 1)%n);
                                    Swap(b,j,(k + j + 1)%n);
                                    Print_State(A,b);
                                }
                            }else{
                                if(A[j][j] != 1){
                                    System.out.println("R" + (j + 1) + " --> " + (1/A[j][j]) + "*R" + (j + 1));
                                    System.out.println();
                                    b[j] /= A[j][j];
                                    A[j][j] = 1;
                                    Print_State(A,b);
                                    v.set(j,(float) (Math.round(b[j] * 10000.0) / 10000.0));
                                }else v.set(j,(float) (Math.round(b[j] * 10000.0) / 10000.0));
                            }
                        }
                    }else{
                        boolean flag = true;
                        System.out.println("row = " + (j + 1) + " ,col = " + (i + 1));
                        float c = A[j][i] / A[i][i];
                        if(c > 0) System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " - " + c + "*R" + (i + 1) + "\n");
                        if(c < 0) System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " + " + (-1)*c + "*R" + (i + 1) + "\n");
                        for (int k = 0 ;k < n ;k++) {
                            A[j][k] = A[j][k] - A[i][k] * c;
                            if(i == k && A[j][k] != 0) A[j][k] = 0;
                            if(flag){
                                b[j] = b[j] - b[i] * c;
                                flag = false;
                            }
                        }
                        Print_State(A,b);
                        if(Is_Unit_Vector(A,j))
                            if(A[j][j] != 1){
                                System.out.println("R" + (j + 1) + " --> " + (1/A[j][j]) + "*R" + (j + 1) + "\n");
                                b[j] /= A[j][j];
                                A[j][j] = 1;
                                Print_State(A,b);
                                v.set(j,(float) (Math.round(b[j] * 10000.0) / 10000.0));
                            }else v.set(j,(float) (Math.round(b[j] * 10000.0) / 10000.0));
                    }
                }else {
                    if(Is_Unit_Vector(A,j))
                        if(A[j][j] != 1){
                            System.out.println("R" + (j + 1) + " --> " + (1/A[j][j]) + "*R" + (j + 1) + "\n");
                            b[j] /= A[j][j];
                            A[j][j] = 1;
                            Print_State(A,b);
                            v.set(j,(float) (Math.round(b[j] * 10000.0) / 10000.0));
                        }else v.set(j,(float) (Math.round(b[j] * 10000.0) / 10000.0));
                    continue;
                }
            }
        }
        return v;
    }

    public static void Check_System(float[][]A ,float[]b) throws Exception{
        if(Is_Square_Matrix(A) && Is_Length_Equals(A,b)){
            Display_Exercise(A,b);
            int n = b.length;
            Vector<Float> x = null;
            if(n > 1){
                Menu_User();
                Scanner sc = new Scanner(System.in);
                int op = sc.nextInt();
                if(Determine(A) == 0) {
                    if(Is_Zero_Vector(b)){
                        if(Is_Zero_Matrix(A)) System.out.print("exists infinite number of solutions to the system under space R" + n);
                        else{
                            System.out.println("det(A) = 0 ,but system homogeneus");
                            A = Ranking_Matrix(A);
                            for(int i = 0 ;i < n - 1 ;i++)
                                b[i] = (-1) * A[i][n - 1];
                            System.out.println("solve the next system: (suppose that: x" + (n) + " = " + 1 + ")\n");
                            switch (op) {
                                case 1:
                                    x = Invertible_Method(Sub_Matrix(A, n - 1, n - 1), Sub_Vector(b, n - 1));
                                    break;
                                case 2:
                                    x = Cramer_Method(Sub_Matrix(A, n - 1, n - 1), Sub_Vector(b, n - 1));
                                    break;
                                case 3:
                                    x = Forward_Backward_Method(Sub_Matrix(A, n - 1, n - 1), Sub_Vector(b, n - 1));
                                    break;
                                case 4:
                                    x = Ranking_Method(Sub_Matrix(A, n - 1, n - 1), Sub_Vector(b, n - 1));
                                    break;
                                default:
                                    System.out.print("enter error number ,goodbye");
                                    System.exit(0);
                            }
                            System.out.println("now adding the x" + (n) + " to solution:\n");
                            x.add((float) 1.0);
                            System.out.print("solution is infinite set of vectors that linear dependent in base " + x);
                        }
                    }else System.out.print("does not exists solutions: det(A) = 0");
                }else{
                    switch (op){
                        case 1:
                            x = Invertible_Method(A,b);
                            break;
                        case 2:
                            x = Cramer_Method(A,b);
                            break;
                        case 3:
                            x = Forward_Backward_Method(A,b);
                            break;
                        case 4:
                            x = Ranking_Method(A,b);
                            break;
                        default:
                            System.out.println("enter error number ,goodbye");
                            System.exit(0);
                    }
                    System.out.print("exist single solution for the system that is: x = " + x);
                }
            }else{ // n == 1
                if(A[0][0] == 0){
                    if(b[0] == 0) System.out.print("exists infinite number of solutions to the equation under space R1");
                    else System.out.print("does not exists any solution to the equation");
                }else System.out.print("exist single solution for the equation that is: x = " + b[0]/A[0][0]);
            }
        }else throw new Exception("error: The input is not exist conditions to linear equations");
    }

    public static void main(String[] args) {
        float[][] A = {{2,1,-1},{-3,-1,2},{-2,1,2}};
        float[] a = {8,-11,-3};
        float[][] B = {{-2,2,2,-1,1},{-4,4,4,4,3},{2,3,2,3,2},{-3,-1,1,2,2},{5,5,3,5,5}};
        float[] b = {1,31,15,8,52};
        float[][] C = {{-2,3,3,-2},{-1,4,2,-2},{1,3,1,3},{-3,-2,4,-5}};
        float[] c = {8,5,19,-19};
        float[][] D = {{6,1,3,3,-11,1},{11,-6,5,11,0,-4},{-2,-2,-4,2,-3,0},{2,12,-1,-7,3,0},{5,-11,-11,8,-8,-2},{3,2,2,-1,-1,1}};
        float[] d = {-1,41,-30,-8,-27,18};
        float[][] E = {{-2,0,-4,-2,5,-6},{6,-1,-2,0,1,0},{6,0,0,-2,5,2},{-2,0,3,2,2,3},{-4,-2,3,-6,4,-4},{5,1,-5,-1,5,-2}};
        float[] e = {0,0,0,0,0,0};
        float[][] F = {{2}};
        float[] f = {4};
        try {
            Check_System(D,d);
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
