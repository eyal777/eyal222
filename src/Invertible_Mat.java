
public class Invertible_Mat {

    /////////////////////////////////////////////////////////////////////

    private static float[][] Upper_Ranking(float[][] mat ,float[][] unit){
        System.out.println("Upper Ranking:");
        System.out.println();
        int n = mat.length;
        Print_State(mat,unit);
        for (int i = 0 ;i < n - 1 ;i++) {
            if(Is_Lower_Triangular(mat)){
                System.out.println("R" + (i + 1) + " --> " + (1/mat[i][i]) + "*R" + (i + 1));
                System.out.println();
                for(int j = 0 ;j < n ;j++){
                    unit[i][j] /= mat[i][i];
                }
                mat[i][i] = 1;
                Print_State(mat,unit);
            }
            for (int j = i + 1 ;j < n ;j++) {
                System.out.println("row = " + (j + 1) + " ,col = " + (i + 1));
                if(mat[i][i] == 0){
                    System.out.println("R" + (i + 1) + " <--> R" + (j + 1));
                    Retreat_Rows(mat,i,j);
                    Retreat_Rows(unit,i,j);
                }
                float c = mat[j][i] / mat[i][i];
                if(c > 0)
                    System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " - " + c + "*R" + (i + 1));
                if(c < 0)
                    System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " + " + (-1)*c + "*R" + (i + 1));
                System.out.println();
                for (int k = 0 ;k < n ;k++) {
                    mat[j][k] = mat[j][k] - mat[i][k] * c;
                    unit[j][k] = unit[j][k] - unit[i][k] * c;
                }
                Print_State(mat,unit);
            }
        }
        if(Is_Upper_Triangular(mat) && Is_Lower_Triangular(mat)){
            System.out.println("R" + (n) + " --> " + (1/mat[n - 1][n - 1]) + "*R" + (n));
            System.out.println();
            for(int j = 0 ;j < n ;j++){
                unit[n - 1][j] /= mat[n - 1][n - 1];
            }
            mat[n - 1][n - 1] = 1;
            Print_State(mat,unit);
        }else{
            unit = Lower_Ranking(mat, unit);
        }
        return unit;
    }

    private static float[][] Lower_Ranking(float[][] mat ,float[][] unit){
        System.out.println("Lower Ranking:");
        System.out.println();
        int n = mat.length;
        Print_State(mat,unit);
        for (int i = n - 1 ;i > 0 ;i--) {
            if(Is_Upper_Triangular(mat)){
                System.out.println("R" + i + " --> " + (1/mat[i][i]) + "*R" + i);
                System.out.println();
                for(int j = 0 ;j < n ;j++){
                    unit[i][j] /= mat[i][i];
                }
                mat[i][i] = 1;
                Print_State(mat,unit);
            }
            for (int j = i - 1 ;j >= 0 ;j--) {
                System.out.println("row = " + (j + 1) + " ,col = " + (i + 1));
                if(mat[i][i] == 0){
                    System.out.println("R" + (i + 1) + " <--> R" + (j + 1));
                    Retreat_Rows(mat,i,j);
                    Retreat_Rows(unit,i,j);
                }
                float c = mat[j][i] / mat[i][i];
                if(c > 0)
                    System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " - " + c + "*R" + (i + 1));
                if(c < 0)
                    System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " + " + (-1)*c + "*R" + (i + 1));
                System.out.println();
                for (int k = n - 1 ;k >= 0 ;k--) {
                    mat[j][k] = mat[j][k] - mat[i][k] * c;
                    unit[j][k] = unit[j][k] - unit[i][k] * c;
                }
                Print_State(mat,unit);
            }
        }
        if(Is_Upper_Triangular(mat) && Is_Lower_Triangular(mat)){
            System.out.println("R" + 1 + " --> " + (1/mat[0][0]) + "*R" + 1);
            System.out.println();
            for(int j = 0 ;j < n ;j++){
                unit[0][j] /= mat[0][0];
            }
            mat[0][0] = 1;
            Print_State(mat,unit);
        }else{
            unit = Upper_Ranking(mat, unit);
        }
        return unit;
    }

    /////////////////////////////////////////////////////////////////////

    private static void Print_State(float[][] mat ,float[][] unit) {
        for (int i = 0 ;i < mat.length ;i++) {
            for (int j = 0 ;j < mat[0].length ;j++) {
                System.out.print(Math.round(mat[i][j] * 1000000.0) / 1000000.0 + " ");
            }
            System.out.print("| ");
            for (int j = 0 ;j < unit[0].length ;j++) {
                System.out.print(Math.round(unit[i][j] * 1000000.0) / 1000000.0 + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void Retreat_Rows(float[][] mat ,int r1 ,int r2){
        for(int j = 0 ;j < mat[0].length ;j++){
            float t = mat[r1][j];
            mat[r1][j] = mat[r2][j];
            mat[r2][j] = t;
        }
    }

    public static float[][] Unit_Matrix(int n) {
        float[][] unit = new float[n][n];
        for (int i = 0 ;i < unit.length ;i++)
            for (int j = 0 ;j < unit[0].length ;j++) {
                if (i == j)
                    unit[i][j] = 1;
                else
                    unit[i][j] = 0;
            }
        return unit;
    }

    public static float[][] Transpose(float[][] mat) {
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < i ;j++){
                float t = mat[j][i];
                mat[j][i] = mat[i][j];
                mat[i][j] = t;
            }
        return mat;
    }

    public static float Determine(float[][] mat) {
        if (mat.length == 1)
            return mat[0][0];
        float sum = 0;
        int k = 1;
        for (int i = 0 ;i < mat.length ;i++) {
            sum += mat[0][i] * Determine(Sub_Matrix(mat, 0, i)) * k;
            k = k * (-1);
        }
        return sum;
    }

    public static float[][] Sub_Matrix(float[][] mat, int x, int y) {
        int n = mat.length ,p = 0 ,q = 0;
        float[][] subMat = new float[n - 1][n - 1];
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < mat[0].length ;j++)
                if (i != x && j != y) {
                    subMat[p][q] = mat[i][j];
                    q++;
                    if (q == subMat[0].length) {
                        p++;
                        q = 0;
                    }
                }
        return subMat;
    }

    public static float[][] Adjoint(float[][] mat) {
        int n = mat.length;
        float[][] matTran = new float[n][n];
        for (int i = 0 ;i < n ;i++)
            for (int j = 0 ;j < n ;j++) {
                if ((i + j) % 2 == 0)
                    matTran[i][j] = Determine(Sub_Matrix(mat, i, j));
                else
                    matTran[i][j] = (-1) * Determine(Sub_Matrix(mat, i, j));
            }
        return Transpose(matTran);
    }

    public static void Print_Matrix(float[][] mat) {
        for (int i = 0 ;i < mat.length ;i++) {
            for (int j = 0 ;j < mat[0].length ;j++) {
                System.out.print(Math.round(mat[i][j] * 1000000.0) / 1000000.0 + " ");
            }
            System.out.println();
        }
    }

    public static float[][] Mult_Const_Matrix(float k ,float[][] mat){
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < mat[0].length ;j++)
                mat[i][j] = k * mat[i][j];
        return mat;
    }

    private static boolean Is_Upper_Triangular(float[][] mat){
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < mat[0].length ;j++)
                if (i > j)
                    if (mat[i][j] != 0)
                        return false;
        return true;
    }

    private static boolean Is_Lower_Triangular(float[][] mat){
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < mat[0].length ;j++)
                if (i < j)
                    if (mat[i][j] != 0)
                        return false;
        return true;
    }

    public static float[][] Invertible(float[][] M){
        System.out.println("Matrix Ranking:");
        System.out.println();
        int n = M.length;
        float[][] invM = Unit_Matrix(n);
        Print_State(M,invM);
        for (int i = 0 ;i < n ;i++) {
            for (int j = 0 ;j < n ;j++) {
                if(M[i][i] == 0){
                    System.out.println("R" + (i + 1) + " <--> R" + (j + i + 2));
                    Retreat_Rows(M,i,j + i + 1);
                    Retreat_Rows(invM,i,j + i + 1);
                    System.out.println();
                    Print_State(M,invM);
                }
                if(i != j){
                    System.out.println("row = " + (j + 1) + " ,col = " + (i + 1));
                    float c = M[j][i] / M[i][i];
                    if(c > 0)
                        System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " - " + c + "*R" + (i + 1));
                    if(c < 0)
                        System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " + " + (-1)*c + "*R" + (i + 1));
                    System.out.println();
                    for (int k = 0 ;k < n ;k++) {
                        M[j][k] = M[j][k] - M[i][k] * c;
                        if(i == k && M[j][k] != 0)
                            M[j][k] = 0;
                        invM[j][k] = invM[j][k] - invM[i][k] * c;
                    }
                    Print_State(M,invM);
                }else{
                    continue;
                }
            }
        }
        if(Is_Upper_Triangular(M) && Is_Lower_Triangular(M)){
            for(int i = 0 ;i < n ;i++){
                System.out.println("R" + (i + 1) + " --> " + (1/M[i][i]) + "*R" + (i + 1));
                System.out.println();
                for(int j = 0 ;j < n ;j++){
                    invM[i][j] /= M[i][i];
                }
                M[i][i] = 1;
                Print_State(M,invM);
            }
        }
        return invM;
    }

    public static float[][] Invertible_Direct(float[][] M) {
        int n = M.length;
        float[][] invM = new float[n][n];
        float[][] adj = Adjoint(M);
        for (int i = 0 ;i < n ;i++)
            for (int j = 0 ;j < n ;j++)
                invM[i][j] = (1 / Determine(M)) * adj[i][j];
        return invM;
    }

    public static float[][] Invertible_Direct_Plus(float[][] M) {
        return Mult_Const_Matrix(1/Determine(M),Adjoint(M));
    }

    public static void main(String[] args) {
        float[][] A = {{2,1,-1},{-3,-1,2},{-2,1,2}};
        float[][] B = {{-2,2,2,-1,1},{-4,4,4,4,3},{2,3,2,3,2},{-3,-1,1,2,2},{5,5,3,5,5}};
        float[][] C = {{-2,3,3,-2},{-1,4,2,-2},{1,3,1,3},{-3,-2,4,-5}};
        float[][] D = {{2,10,7,-5,0,8},{-12,-8,-9,-1,-11,-5},{-1,8,0,-4,-9,11},{12,6,-10,5,-11,-12},{12,11,-10,-2,-4,-12},{5,-5,3,6,8,-10}};
        float[][] E = {{1,1,-2,0,-1,-1},{0,-2,-1,2,-2,2},{0,0,-1,-1,-1,1},{0,0,-2,2,-2,0},{-2,-2,0,0,-2,2},{1,0,-1,0,0,2}};
        float[][] F = {{-2,0,-4,-2,5,-6},{6,-1,-2,0,1,0},{6,0,0,-2,5,2},{-4,0,6,4,4,6},{-4,-2,3,-6,4,-4},{5,1,-5,-1,5,-2}};
        //Print_Matrix(Invertible_Direct(E));
        System.out.println("Find The Next Matrix's Invertible:");
        System.out.println();
        Print_Matrix(E);
        System.out.println();
        float[][] invE = Invertible(E);
        Print_Matrix(invE);
    }
}
