import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;
import java.util.Vector;

public class System_Linear_Equations_2 {

    ///////////////////////////Print/////////////////////////////
    public static void Display_Exercise(float[][] A ,float[] b) {
        System.out.println("Solve The Next Exercise (" + A.length + " Variables):");
        if(A[0].length == 1) System.out.println(A[0][0] + "*x = " + b[0]);
        else
            for (int i = 0 ;i < A.length ;i++) {
                boolean flag = true;
                for (int j = 0 ;j < A[0].length ;j++) {
                    if(A[i][j] != 0){
                        if(A[i][j] > 0){
                            if(flag) {
                                System.out.print("");
                                flag = false;
                            }else System.out.print(" +");
                            if(Math.abs(A[i][j]) == 1) System.out.print("x" + (j + 1));
                            else System.out.print((int)A[i][j] + "*x" + (j + 1));
                        }else{
                            if(flag) {
                                System.out.print("-");
                                flag = false;
                            }else System.out.print(" -");
                            if(Math.abs(A[i][j]) == 1) System.out.print("x" + (j + 1));
                            else System.out.print((int)Math.abs(A[i][j]) + "*x" + (j + 1));
                        }
                    }else continue;
                }
                System.out.println(" = " + (int)b[i]);
            }
        System.out.println();
    }

    private static void Print_State(float[][] A ,float[] b) {
        for (int i = 0 ;i < A.length ;i++) {
            for (int j = 0 ;j < A[0].length ;j++)
                System.out.print(Math.round(A[i][j] * 1000.0) / 1000.0 + " ");
            System.out.println("| " + Math.round(b[i] * 1000.0) / 1000.0);
        }
        System.out.println();
    }

    public static void Print_Vector(float[] x){
        String s = "x = (";
        for (int i = 0 ;i < x.length ;i++)
            if(i == x.length - 1) s += Math.round(x[i] * 1000.0) / 1000.0 + ")";
            else s += Math.round(x[i] * 1000.0) / 1000.0 + ", ";
        System.out.println(s);
    }

    //////////////////////////////Auxiliary Operations////////////////////////////
    private static float[][] Mult_Mats(float[][] M1, float[][] M2) throws Exception{
        if(M1[0].length == M2.length){
            float[][] A = new float[M1.length][M2[0].length];
            for (int i = 0 ;i < M1.length ;i++)
                for (int j = 0 ;j < M2[0].length ;j++)
                    for (int k = 0 ;k < M2.length ;k++)
                        A[i][j] += M1[i][k] * M2[k][j];
            return A;
        }else throw new Exception ("Error: The sizes Required is not Conditions");
    }

    private static void Retreat_Rows_System(float[][] A ,float[] b ,int r1 ,int r2){
        for(int j = 0 ;j < A[0].length ;j++){
            float t = A[r1][j];
            A[r1][j] = A[r2][j];
            A[r2][j] = t;
        }
        float t = b[r1];
        b[r1] = b[r2];
        b[r2] = t;
    }

    private static boolean Is_Unit_Vector(float[][] A ,int k){
        boolean flag = true;
        for(int i = 0 ;i < A.length ;i++) {
            if (A[k][i] != 0) {
                for (int j = 0; j < A.length; j++) {
                    if (j == i) continue;
                    else {
                        if (A[k][j] == 0) flag = true;
                        else {
                            flag = false;
                            break;
                        }
                    }
                }
                if (flag) break;
            }else continue;
        }
        return flag;
    }

    private static boolean Is_Unit_Matrix(float[][] A) {
        int n = A.length;
        boolean flag = true;
        for (int i = 0 ;i < n ;i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    if (A[i][j] == 1) flag = true;
                    else {
                        flag = false;
                        break;
                    }
                } else {
                    if (A[i][j] == 0) flag = true;
                    else {
                        flag = false;
                        break;
                    }
                }
            }
            if(!flag) break;
        }
        return flag;
    }

    public static float[][] Unit_Matrix(int n) {
        float[][] unit = new float[n][n];
        for (int i = 0 ;i < unit.length ;i++)
            for (int j = 0 ;j < unit[0].length ;j++) {
                if (i == j)
                    unit[i][j] = 1;
                else
                    unit[i][j] = 0;
            }
        return unit;
    }

    private static int Get_Index_UnZero(float[][] A ,int k){
        for(int i = k + 1 ;i < A.length ;i++)
            if(A[i][k] != 0) return i;
        return -1;
    }

    private static boolean Is_Upper_Triangular(float[][] mat) {
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < mat[0].length ;j++)
                if (i > j)
                    if (mat[i][j] != 0)
                        return false;
        return true;
    }

    private static boolean Is_Lower_Triangular(float[][] mat) {
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < mat[0].length ;j++)
                if (i < j)
                    if (mat[i][j] != 0)
                        return false;
        return true;
    }

    private static float[][] Copy_Matrix(float[][] A){
        float[][] copyA = new float[A.length][A.length];
        for (int i = 0 ;i < A.length ;i++)
            for (int j = 0 ;j < A[0].length ;j++)
                copyA[i][j] = A[i][j];
        return copyA;
    }

    private static float[] Copy_Vector(float[] b){
        float[] c = new float[b.length];
        for (int i = 0 ;i < c.length ;i++)
            c[i] = b[i];
        return c;
    }

    ///////////////////////////SLE Solutions/////////////////////////////
    private static float[] Elem_Met_Rec(float[][] A ,float[] b ,int i ,int j) throws Exception {
        int n = A.length;
        if(Is_Unit_Matrix(A)) return b;
        else {
            float[][] E = Unit_Matrix(n);
            if (A[i][i] == 0) {
                int k = Get_Index_UnZero(A,i);
                System.out.println("R" + (i + 1) + " <--> R" + (k % n + 1) + "\n");
                Retreat_Rows_System(E ,b ,i ,k % n);
                A = Mult_Mats(E, A);
                Print_State(A, b);
            } else {
                if (i != j) {
                    if(A[j][i] != 0){
                        float c = A[j][i] / A[i][i];
                        if (c > 0) System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " - " + c + "*R" + (i + 1) + "\n");
                        else System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " + " + (-1) * c + "*R" + (i + 1) + "\n");
                        E[j][i] = (-1) * c;
                        b[j] = b[j] + b[i] * E[j][i];
                        A = Mult_Mats(E, A);
                        if(A[j][i] != 0) A[j][i] = 0;
                        Print_State(A, b);
                    }
                } else {
                    if (Is_Unit_Vector(A, j) && A[j][j] != 1) {
                        System.out.println("R" + (j + 1) + " --> " + (1 / A[j][j]) + "*R" + (j + 1) + "\n");
                        E[j][j] = 1 / A[j][j];
                        b[j] = b[j] * E[j][j];
                        A = Mult_Mats(E, A);
                        if(A[j][j] != 1) A[j][j] = 1;
                        Print_State(A, b);
                    }
                }
                if (j == n - 1) i = (i + 1) % n;
                j = (j + 1) % n;
            }
            return Elem_Met_Rec(A, b, i, j);
        }
    }

    private static float[] Elem_Met_Iter(float[][] A ,float[] b) throws Exception {
        int n = A.length;
        int i = 0 ,j = 1;
        while(!Is_Unit_Matrix(A)){
            float[][] E = Unit_Matrix(n);
            if(A[i][i] == 0) {
                int k = Get_Index_UnZero(A,i);
                System.out.println("R" + (i + 1) + " <--> R" + (k % n + 1) + "\n");
                Retreat_Rows_System(E ,b ,i ,k % n);
                A = Mult_Mats(E, A);
                Print_State(A, b);
            }else{
                if(i != j){
                    if(A[j][i] != 0){
                        float c = A[j][i] / A[i][i];
                        if (c > 0) System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " - " + c + "*R" + (i + 1) + "\n");
                        else System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " + " + (-1) * c + "*R" + (i + 1) + "\n");
                        E[j][i] = (-1) * c;
                        b[j] = b[j] + b[i] * E[j][i];
                        A = Mult_Mats(E, A);
                        if(A[j][i] != 0) A[j][i] = 0;
                        Print_State(A,b);
                    }
                }else{
                    if(Is_Unit_Vector(A, j) && A[j][j] != 1){
                        System.out.println("R" + (j + 1) + " --> " + (1 / A[j][j]) + "*R" + (j + 1) + "\n");
                        E[j][j] = 1 / A[j][j];
                        b[j] = b[j] * E[j][j];
                        A = Mult_Mats(E, A);
                        if(A[j][j] != 1) A[j][j] = 1;
                        Print_State(A,b);
                    }
                }
                if(j == n - 1) i = (i + 1)%n;
                j = (j + 1)%n;
            }
        }
        return b;
    }

    ///////////////////////////Test Solutions/////////////////////////////
    public static boolean Correct_Cheking(float[][] A ,float[] b) throws Exception {
        int n = A.length;
        double eps = 0.1;
        float[] c = Copy_Vector(b);
        b = Elem_Met_Iter(A,b);
        float fx = 0;
        boolean flag = true;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                fx += A[i][j] * b[j];
            if(fx >= c[i] - eps && fx <= c[i] + eps){
                flag = true;
                fx = 0;
            }else{
                flag = false;
                break;
            }
        }
        return flag;
    }

    ///////////////////////////Check System/////////////////////////////
    private static boolean Is_Zero_Matrix(float[][] A){
        boolean flag = true;
        for(int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                if(A[i][j] == 0) flag = true;
                else{
                    flag = false;
                    break;
                }
            }
            if(!flag) break;
        }
        return flag;
    }

    private static boolean Is_Zero_Vector(float[] b){
        boolean flag = true;
        for(int i = 0; i < b.length; i++){
            if(b[i] == 0) flag = true;
            else{
                flag = false;
                break;
            }
        }
        return flag;
    }

    private static boolean Is_Square_Matrix(float[][] A) {
        return A.length == A[0].length;
    }

    private static boolean Is_Length_Equals(float[][] A ,float[] b){ return A.length == b.length && A[0].length == b.length; }

    public static float Determine(float[][] A) {
        if (A.length == 1) return A[0][0];
        float sum = 0;
        int k = 1;
        for (int i = 0 ;i < A.length ;i++) {
            sum += A[0][i] * Determine(Sub_Matrix(A, 0, i)) * k;
            k = k * (-1);
        }
        return sum;
    }

    public static float[][] Sub_Matrix(float[][] A, int x, int y) {
        int n = A.length ,p = 0 ,q = 0;
        float[][] subA = new float[n - 1][n - 1];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != x && j != y) {
                    subA[p][q] = A[i][j];
                    q++;
                    if (q == subA[0].length) {
                        p++;
                        q = 0;
                    }
                }
            }
        }
        return subA;
    }

    public static void Sys_Invest(float[][] A ,float[] b) throws Exception {
        if(Is_Square_Matrix(A) && Is_Length_Equals(A,b)){
            Display_Exercise(A,b);
            int n = b.length;
            if(n > 1){
                if(Determine(A) == 0) {
                    if(Is_Zero_Vector(b)){
                        if(Is_Zero_Matrix(A)) System.out.print("exists infinite number of solutions to the system under space R" + n);
                        else System.out.print("solution is infinite set of vectors that linear dependent in base x");
                    }else System.out.print("does not exists solutions: det(A) = 0");
                }else System.out.print("vector x is single solution for system Ax = b");
            }else{ // n == 1
                if(A[0][0] == 0){
                    if(b[0] == 0) System.out.print("exists infinite number of solutions to the equation under space R1");
                    else System.out.print("does not exists any solution to the equation");
                }else System.out.print("exist single solution for the equation that is: x = b/a");
            }
        }else throw new Exception("error: The input is not exist conditions to linear equations");
    }

    public static String Ret_Num_Vec(float[][] A ,float[] b) throws Exception {
        if(Is_Square_Matrix(A) && Is_Length_Equals(A,b)){
            Display_Exercise(A,b);
            if(Determine(A) == 0) {
                if(Is_Zero_Vector(b)){
                    if(Is_Zero_Matrix(A)) return "Global Infinity";
                    else return "Local Infinity";
                }else return "Zero";
            }else return "One";
        }else throw new Exception("error: The input is not exist conditions to linear equations");
    }

    ///////////////////////////Main/////////////////////////////
    public static void main(String[] args) {
        float[][] A1 = {{6,1,3,3,-11,1},{11,-6,5,11,0,-4},{-2,-2,-4,2,-3,0},{2,12,-1,-7,3,0},{5,-11,-11,8,-8,-2},{3,2,2,-1,-1,1}};
        float[] b1 = {-1,41,-30,-8,-27,18};
        // x = (4.8, -3.7, 3.3, -3.1, 2.8, 4.1)
        float[][] A2 = {{7,7,-2,4,-12,-12},{0,6,-8,12,-9,4},{5,-10,-2,-3,-5,1},{6,1,-4,-6,-4,-3},{3,-2,-7,-10,0,1},{1,11,4,-9,10,-1}};
        float[] b2 = {49,39,48,27,-52,18};
        // x = (5.6, 7, 19.6, -6.6, -18.6, 16.4)
        float[][] A3 = {{9,-3,-11,-3,0,-12},{3,0,5,-5,-1,-3},{3,0,12,-9,-5,-11},{8,-10,3,1,-7,-12},{7,-6,-1,-10,6,-5},{-4,8,-10,2,0,-6}};
        float[] b3 = {5,-37,-19,-25,31,41};
        // x = (-26.7, -18.7, -17.2, -25.2, -22, 6.3)
        float[][] A4 = {{-8,6,-2,9,5,0},{-7,1,-12,7,12,1},{2,-1,4,4,-12,-2},{-2,1,6,8,-12,0},{-3,6,-9,4,-7,-2},{6,-8,-6,-3,7,-2}};
        float[] b4 = {55,27,14,16,8,-13};
        // x = (-21, -9, 3.5, -5.5, -0.5, -24.5)
        float[][] A5 = {{10,8,-4,2,10,0},{-4,0,4,12,9,-9},{-5,11,-11,9,-1,-3},{-6,5,-4,2,-5,2},{-8,3,8,9,10,3},{12,0,10,-11,-9,-1}};
        float[] b5 = {5,-5,-10,-5,1,-28};
        // x = (-8.5, 4, -1.5, -14, 8, -7)
        float[][] A6 = {{2,10,7,-5,0,8},{-12,-8,-9,-1,-11,-5},{-1,8,0,-4,-9,11},{12,6,-10,5,-11,-12},{12,11,-10,-2,-4,-12},{5,-5,3,6,8,-10}};
        float[] b6 = {-45,-13,-23,36,-43,28};
        // x = (49, -55, 16, -51, -23, 5)
        float[][] A7 = {{0,0,-1,-6,-1,0},{0,-3,-1,-1,3,0},{0,-4,-4,-4,1,0},{-3,-4,1,0,5,0},{-1,0,4,6,3,2},{1,4,2,6,-2,-2}};
        float[] b7 = {-4,15,-1,-6,27,-10};
        // x = (42.0, -75.0, 65.0, -2.0, -49.0, -16.0)
        float[][] A8 = {{-5,0,-1,9,-7,5},{11,9,1,-2,-10,-12},{-3,4,-2,-5,1,-1},{1,-3,5,4,-2,0},{-3,7,0,-6,-11,-7},{5,-12,3,7,9,6}};
        float[] b8 = {-13,-24,6,-2,10,9};
        // x = (42.5, 14.5, 27.0, -50.5, -33.0, 90.0)
        float[][] A9 = {{9,0,3,-4,5,7},{-11,-2,1,-6,-7,-12},{3,6,-2,-2,-7,-12},{-8,-3,0,-12,-4,1},{10,7,5,9,12,5},{10,3,-5,-5,-6,0}};
        float[] b9 = {-14,-39,-7,40,22,0};
        // x = (-8.5, 51, 147, 46, -149.5, 79)
        float[][] A10 = {{-6,-7,-8,-12,12,-6},{-4,-10,-10,-4,-10,6},{-5,6,7,8,-1,-2},{-10,-5,-10,-12,9,1},{4,-1,-8,-12,9,2},{10,10,-7,-7,2,0}};
        float[] b10 = {5,-37,-20,32,-20,-6};
        // x = (-40.5, 32, 206.5, -255.5, -132, -26)
        float[][] A11 = {{-2,-9,-10,0,2,-10},{-3,-11,-10,12,12,-8},{0,9,4,-2,0,-2},{-3,-6,-6,-3,0,-7},{0,5,3,11,10,-3},{2,9,12,6,3,-2}};
        float[] b11 = {-36,-10,-17,-13,39,3};
        // x = (271, -105, 95, -294.5, 358, 20.5)
        float[][] A12 = {{11,-5,0,0,0,8},{-3,8,2,1,-4,3},{-8,12,-12,8,-9,-3},{4,-11,-9,4,11,-5},{12,-6,4,-3,0,2},{2,11,4,0,-9,-2}};
        float[] b12 = {41,-30,-36,-22,10,-19};
        // x = (114, 329, -413, -720, 234, 54)
        float[][] A13 = {{10,-1,9,0,1,8},{7,-1,-9,8,-10,-1},{-5,3,-10,-3,-5,0},{-10,0,0,6,9,-7},{-8,8,-9,-6,3,-2},{1,10,3,-8,12,-1}};
        float[] b13 = {36,32,21,-33,34,-52};
        // x = (669, -1750, -1378, -582, 1382, 327)
        float[][] A14 = {{2,-2,-3,6,-5,-3},{-3,2,2,2,2,1},{2,-1,0,6,-1,0},{6,4,4,-2,5,2},{0,-5,3,-1,4,-3},{3,1,0,0,0,1}};
        float[] b14 = {11,8,6,24,-3,-5};
        // x = (21.5, 287, -2727.5, 415, 2240, -356.5)
        float[][] A15 = {{0,-5,-5,2,-5,-1},{0,-3,-3,-1,1,0},{4,2,-3,5,3,-2},{3,0,1,-6,5,3},{0,2,3,0,-3,0},{-4,3,5,-6,5,0}};
        float[] b15 = {4,11,-8,11,-3,-18};
        // is incorrect
        // x = (-2027, 6327, -6220, -2333, -2001, 2773)

        // x = (2735, -1189, -1723, 2518, -3501, 3473)

        // x = (-3653.6, -2689.2, -2276, 5182.2, 6927.4, 1951.4)

        // x = (-2569.5, -1437, 1404, -3543.5, -776.5, 4346.5)

        // x = (3088, 928, 4430, -4623, -2051, 2519)
        float[][] A20 = {{-6,4,9,-7,0,-2},{-5,11,-3,-1,-10,0},{6,5,-12,-3,12,-1},{7,0,0,4,8,2},{8,-8,12,2,-7,4},{12,-1,0,7,-11,6}};
        float[] b20 = {17,-24,46,10,46,35};
        // x = (836, 231, 289, 618, -310, -2917)
        float[][] A21 = {{-2,-9,-10,0,2,-10},{-3,-11,-10,12,12,-8},{0,9,4,-2,0,-2},{-3,-6,-6,-3,0,-7},{0,5,3,11,10,-3},{2,9,12,6,3,-2}};
        float[] b21 = {-36,-10,-17,-13,39,3};
        // x = (271, -105, 95, -294.5, 358, 20.5)
        float[][] A22 = {{-11,-5,-10,9,-3,-3},{2,-1,10,-1,-7,-4},{-7,-12,-7,0,-7,-1},{7,1,-5,-2,1,-9},{0,-6,-6,1,-6,-12},{-1,-7,8,-10,-3,-1}};
        float[] b22 = {60,-28,-46,-32,18,-2};
        // x = (-558.5, 1153, -490, -870, -930, 59.5)
        float[][] A23 = {{-12,-9,-9,-3,-2,12},{2,0,3,-1,3,7},{3,-2,-5,-6,-6,-1},{-10,11,4,-3,8,3},{10,-8,11,5,6,0},{-10,11,-8,1,-4,2}};
        float[] b23 = {-13,52,-13,-16,-58,-19};
        // x = (56, 7, -122, 6, 125, -9.0)
        float[][] A24 = {{-11,-11,-7,12,-9,-4},{-4,11,-4,12,8,1},{-6,3,3,0,11,5},{6,-1,1,-3,0,-6},{-3,-1,-6,12,-11,0},{-11,-7,-8,7,-4,-3}};
        float[] b24 = {-9,-5,-25,16,-39,-70};
        //  x = (-1923.8, 1729.6, 1866.8, -602, -1304, -1602.6)
        float[][] A25 = {{-4,0,-3,1,-9,0},{-5,0,1,-6,-8,-10},{-8,-1,-12,-4,-8,-6},{11,3,2,4,4,-11},{-7,-1,-9,-4,-10,-4},{10,1,-11,12,-8,9}};
        float[] b25 = {5,-7,-51,-59,52,-36};
        // x = (-4, 671, -41, -179, -5, 110)
        float[][] A26 = {{-1,4,-12,12,3,-1},{3,-1,2,10,2,1},{10,2,5,0,0,5},{-9,-8,6,3,0,-1},{12,0,-3,8,0,2},{-4,0,3,-7,-1,-12}};
        float[] b26 = {-24,-20,23,54,-4,-60};
        // x = (388.2, -966, -350, -705.2, 2818.4, -35.4)
        float[][] A27 = {{6,-8,-2,4,4,-11},{1,-2,9,0,-7,12},{4,-10,8,9,3,-8},{-6,-11,6,0,10,-8},{-7,-2,-6,-7,5,-12},{7,-7,3,6,3,-3}};
        float[] b27 = {33,19,-54,49,-36,-47};
        // x = (-100, -101, -124, 105, -43, 61)
        try {
            Display_Exercise(A1,b1);
            Print_State(A1,b1);
            float[] x = Elem_Met_Iter(A1,b1);
            //float[] x = Elem_Met_Rec(A1,b1,0,1);
            Print_Vector(x);
            //Sys_Invest(A1,b1);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
