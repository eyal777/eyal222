
public class Mat_Decomposition {
    public static void Print_Matrix(float[][] mat) {
        for (int i = 0 ;i < mat.length ;i++) {
            for (int j = 0 ;j < mat[0].length ;j++) {
                System.out.print(Math.round(mat[i][j] * 1000000.0) / 1000000.0 + " ");
            }
            System.out.println();
        }
    }

    private static boolean Is_Square_Matrix(float[][] mat) {
        if (mat.length != mat[0].length)
            return false;
        return true;
    }

    private static boolean Is_Upper_Triangular(float[][] mat) {
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < mat[0].length ;j++)
                if (i > j)
                    if (mat[i][j] != 0)
                        return false;
        return true;
    }

    private static boolean Is_Lower_Triangular(float[][] mat) {
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < mat[0].length ;j++)
                if (i < j)
                    if (mat[i][j] != 0)
                        return false;
        return true;
    }

    private static boolean Is_Symmetrical_Matrix(float[][] mat) {
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < mat[0].length ;j++)
                if (mat[i][j] != mat[j][i])
                    return false;
        return true;
    }

    private static boolean Is_Values_Positives(float[][] mat) {
        for (int i = 0 ;i < mat.length ;i++)
            if (mat[i][i] < 0)
                return false;
        return true;
    }

    private static boolean Is_Transpose_Matrix(float[][] mat1, float[][] mat2) {
        for (int i = 0 ;i < mat1.length ;i++)
            for (int j = 0 ;j < mat1[0].length ;j++)
                if (mat1[i][j] != mat2[j][i])
                    return false;
        return true;
    }

    private static boolean Is_One_Slant(float[][] mat){
        boolean flag = true;
        for(int i = 0 ;i < mat.length ;i++){
            if(mat[i][i] == 1)
                flag = true;
            else{
                flag = false;
                break;
            }
        }
        return flag;
    }

    public static float[][] Mult_Mats(float[][] mat1, float[][] mat2) throws Exception{
        if(mat1[0].length == mat2.length){
            float[][] ans = new float[mat1.length][mat2[0].length];
            for (int i = 0 ;i < mat1.length ;i++)
                for (int j = 0 ;j < mat2[0].length ;j++)
                    for (int k = 0 ;k < mat2.length ;k++)
                        ans[i][j] += mat1[i][k] * mat2[k][j];
            return ans;
        }else{
            throw new Exception ("Error: The sizes Required is not Conditions");
        }
    }

    public static float[][] Transpose(float[][] mat) {
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < i ;j++){
                float t = mat[j][i];
                mat[j][i] = mat[i][j];
                mat[i][j] = t;
            }
        return mat;
    }

    private static void Retreat_Rows(float[][] mat ,int r1 ,int r2){
        for(int j = 0 ;j < mat[0].length ;j++){
            float t = mat[r1][j];
            mat[r1][j] = mat[r2][j];
            mat[r2][j] = t;
        }
    }

    public static void From_LU_To_M(float[][] L, float[][] U) throws Exception{
        if (Is_Square_Matrix(L) && Is_Square_Matrix(U) && Is_Lower_Triangular(L)
                && Is_Upper_Triangular(U) && Is_One_Slant(L)) {
            float[][] M = Mult_Mats(L, U);
            System.out.println("L = ");
            Print_Matrix(L);
            System.out.println();
            System.out.println("U = ");
            Print_Matrix(U);
            System.out.println();
            System.out.println("M = L * U");
            Print_Matrix(M);
        } else
            throw new Exception ("Error: Not all conditions are held");
    }

    public static void From_M_To_LU(float[][] M) throws Exception{
        if (Is_Square_Matrix(M)) {
            int n = M.length;
            float[][] U = Copy_Matrix(M);
            float[][] L = new float[n][n];
            for (int i = 0 ;i < L.length ;i++)
                L[i][i] = 1;
            // Rating Matrix U
            for (int i = 0 ;i < n - 1 ;i++) {
                for (int j = i + 1 ;j < n ;j++) {
                    float c = U[j][i] / U[i][i];
                    L[j][i] = c;
                    for (int k = 0 ;k < n ;k++) {
                        U[j][k] = U[j][k] - U[i][k] * c;
                    }
                }
            }
            System.out.println();
            System.out.println("M = L * U");
            Print_Matrix(M);
            System.out.println();
            System.out.println("L = ");
            Print_Matrix(L);
            System.out.println();
            System.out.println("U = ");
            Print_Matrix(U);
        } else
            throw new Exception ("Error: The matrix is not square");
    }



    public static void From_M_To_LU_2(float[][] M) throws Exception{
        if (Is_Square_Matrix(M)) {
            int n = M.length;
            float[][] U = Ranking_Matrix(M);
            float[][] L = Mult_Mats(M,Invertible(U));
            System.out.println();
            System.out.println("M = L * U");
            Print_Matrix(M);
            System.out.println();
            System.out.println("L = ");
            Print_Matrix(L);
            System.out.println();
            System.out.println("U = ");
            Print_Matrix(U);
        } else
            throw new Exception ("Error: The matrix is not square");
    }

    private static float[][] Copy_Matrix(float[][] mat){
        float[][] copy = new float[mat.length][mat.length];
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < mat[0].length ;j++)
                copy[i][j] = mat[i][j];
        return copy;
    }

    public static void From_LLT_To_M(float[][] L ,float[][] LT) throws Exception{
        if (Is_Transpose_Matrix(L, LT) && Is_Square_Matrix(L) && Is_Square_Matrix(LT)
                && Is_Lower_Triangular(L) && Is_Upper_Triangular(LT)) {
            float[][] M = Mult_Mats(L, LT);
            System.out.println("L = ");
            Print_Matrix(L);
            System.out.println();
            System.out.println("LT = ");
            Print_Matrix(LT);
            System.out.println();
            System.out.println("M = L * LT");
            Print_Matrix(M);
        } else
            throw new Exception ("Error: Not all conditions are held");
    }

    public static void From_M_To_LLT(float[][] M) throws Exception{
        if (Is_Square_Matrix(M) && Is_Symmetrical_Matrix(M)
                && Is_Values_Positives(M)) {
            float[][] LT = M;
            float[][] L = M;
            for (int i = 0 ;i < LT.length - 1 ;i++) {
                for (int j = i + 1 ;j < LT[0].length ;j++) {
                    float c = LT[j][i] / LT[i][i];
                    for (int k = 0 ;k < LT.length ;k++) {
                        LT[j][k] = LT[j][k] - LT[i][k] * c;
                    }
                }
            }
            for (int i = 0 ;i < LT.length ;i++) {
                for (int j = LT.length - 1 ;j >= i ;j--) {
                    if (i == j)
                        LT[i][j] = (float) Math.sqrt(LT[i][i]);
                    else
                        LT[i][j] = (float) (LT[i][j] / Math.sqrt(LT[i][i]));
                }
            }
            L = Transpose(LT);
            System.out.println();
            System.out.println("M = L * LT");
            Print_Matrix(M);
            System.out.println();
            System.out.println("L = ");
            Print_Matrix(L);
            System.out.println();
            System.out.println("LT = ");
            Print_Matrix(LT);
        } else
            throw new Exception ("Error: Not all conditions are held");
    }

    private static float[][] Ranking_Matrix(float[][] mat){
        int n = mat.length;
        float[][] matRank = Copy_Matrix(mat);
        for (int i = 0 ;i < n - 1 ;i++) {
            for (int j = i + 1 ;j < n ;j++) {
                System.out.println("row = " + (j + 1) + " ,col = " + (i + 1));
                if(matRank[i][i] == 0){
                    System.out.println("R" + (i + 1) + " <--> R" + (j + 1));
                    Retreat_Rows(matRank,i,j);
                }
                float c = matRank[j][i] / matRank[i][i];
                if(c > 0)
                    System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " - " + c + "*R" + (i + 1));
                if(c < 0)
                    System.out.println("R" + (j + 1) + " --> R" + (j + 1) + " + " + (-1)*c + "*R" + (i + 1));
                System.out.println();
                for (int k = 0 ;k < n ;k++) {
                    matRank[j][k] = matRank[j][k] - matRank[i][k] * c;
                }
                Print_Matrix(matRank);
                System.out.println();
            }
        }
        return matRank;
    }

    public static float Determine(float[][] mat) {
        if (mat.length == 1)
            return mat[0][0];
        float sum = 0;
        int k = 1;
        for (int i = 0 ;i < mat.length ;i++) {
            sum += mat[0][i] * Determine(Sub_Matrix(mat, 0, i)) * k;
            k = k * (-1);
        }
        return sum;
    }

    public static float[][] Sub_Matrix(float[][] mat, int x, int y) {
        int n = mat.length ,p = 0 ,q = 0;
        float[][] subMat = new float[n - 1][n - 1];
        for (int i = 0 ;i < mat.length ;i++)
            for (int j = 0 ;j < mat[0].length ;j++)
                if (i != x && j != y) {
                    subMat[p][q] = mat[i][j];
                    q++;
                    if (q == subMat[0].length) {
                        p++;
                        q = 0;
                    }
                }
        return subMat;
    }

    public static float[][] Invertible(float[][] mat) {
        int n = mat.length;
        float[][] matInver = new float[n][n];
        float det = Determine(mat);
        float[][] adj = Adjoint(mat);
        for (int i = 0 ;i < n ;i++)
            for (int j = 0 ;j < n ;j++)
                matInver[i][j] = (1 / det) * adj[i][j];
        return matInver;
    }

    public static float[][] Adjoint(float[][] mat) {
        int n = mat.length;
        float[][] matTran = new float[n][n];
        //float[][] matAdj = new float[n][n];
        for (int i = 0 ;i < n ;i++)
            for (int j = 0 ;j < n ;j++) {
                if ((i + j) % 2 == 0)
                    matTran[i][j] = Determine(Sub_Matrix(mat, i, j));
                else
                    matTran[i][j] = (-1) * Determine(Sub_Matrix(mat, i, j));
            }
        float[][] matAdj = Transpose(matTran);
        return matAdj;
    }

    public static void main(String[] args) {
        float[][] A = {{2, 1, -1}, {-3, -1, 2}, {-2, 1, 2}};
        float[][] B = {{-2, 2, 2, -1, 1}, {-4, 4, 4, 4, 3}, {2, 3, 2, 3, 2}, {-3, -1, 1, 2, 2}, {5, 5, 3, 5, 5}};
        float[][] C = {{-2, 3, 3, -2}, {-1, 4, 2, -2}, {1, 3, 1, 3}, {-3, -2, 4, -5}};
        float[][] D = {{6, 1, 3, 3, -11, 1}, {11, -6, 5, 11, 0, -4}, {-2, -2, -4, 2, -3, 0}, {2, 12, -1, -7, 3, 0}, {5, -11, -11, 8, -8, -2}, {3, 2, 2, -1, -1, 1}};
        try {
            From_M_To_LU_2(B);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
